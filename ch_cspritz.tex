\chapter{CSpritz: accurate prediction of protein disorder segments with annotation for homology, secondary structure and linear motifs}
\label{chap:cspritz}

This chapter was first published in Walsh I, Martin AJM, Domenico TD, Vullo A, Pollastri G, et al. (2011) CSpritz: accurate prediction of protein disorder segments with annotation for homology, secondary structure and linear motifs. Nucl Acids Res 39: W190–W196.

\section{Summary}

CSpritz is a web server for the prediction of intrinsic protein disorder. It is a combination of previous Spritz with two novel orthogonal systems developed by our group (Punch and ESpritz). Punch is based on sequence and structural templates trained with support vector machines. ESpritz is an efficient single sequence method based on bidirectional recursive neural networks. Spritz was extended to filter predictions based on structural homologues. After extensive testing, predictions are combined by averaging their probabilities. The CSpritz website can elaborate single or multiple predictions for either short or long disorder. The server provides a global output page, for download and simultaneous statistics of all predictions. Links are provided to each individual protein where the amino acid sequence and disorder prediction are displayed along with statistics for the individual protein. As a novel feature, CSpritz provides information about structural homologues as well as secondary structure and short functional linear motifs in each disordered segment. Benchmarking was performed on the very recent CASP9 data, where CSpritz would have ranked consistently well with a Sw measure of 49.27 and AUC of 0.828. The server, together with help and methods pages including examples, are freely available at URL: \url{http://protein.bio.unipd.it/cspritz/}.

\section{Introduction}

The 3D native structure of proteins has been considered the major determinant of function for many years. Over the last decade there has been a growing realization of an alternative mechanism whereby non-folding regions are both widespread and also carry functional significance \cite{wright_intrinsically_1999,dyson_intrinsically_2005}. These non-folding regions within a protein, coming in various guises ranging from fully extended to molten globule-like and partially folded structures \cite{tompa_fuzzy_2008}, are collectively known as intrinsically disordered regions \cite{tompa_intrinsically_2002}. Such regions often become structured upon binding to a target molecule and have been shown to be involved in various biological processes such as cell signaling or regulation \cite{dunker_intrinsic_2002}, DNA binding \cite{weiss_folding_1990} and molecular recognition in general \cite{tompa_fuzzy_2008,tompa_close_2009}. An interesting observation is that the amount of disorder within a proteome seems to correlate with complexity of the organism, with an apparent increase in disorder for eukaryotic organisms \cite{dunker_intrinsic_2000,ward_prediction_2004}. The conservation of disorder \cite{schaefer_protein_2010,siltberg-liberles_evolution_2011} and specific amino acid patterns \cite{lise_sequence_2005,lobanov_comsin:_2010} (e.g. PxPxP) have also been studied. Indeed, there is a growing realization that intrinsically disordered regions are widely used as hubs for protein–protein interactions \cite{russell_careful_2008}, for which structural data can be accessed in the ComSin database \cite{lobanov_comsin:_2010}. Functional linear motifs \cite{gibson_cell_2009,diella_understanding_2008}, which are mostly hidden in disordered regions \cite{fuxreiter_local_2007}, have been characterized in resources such as ELM \cite{gould_elm:_2010}, an online repository of linear motifs.

The experimental determination of native disorder, once considered an anomaly, can be time consuming, difficult and expensive. As a result, computational approaches have largely driven our understanding of disorder over the last decade \cite{russell_careful_2008}. The bi-yearly Critical Assessment of Techniques for protein Structure Prediction (CASP) experiment has included a disorder category since CASP5 in 2002 \cite{melamud_evaluation_2003}. Previously published methods can be roughly divided into biophysical and machine learning approaches. The former rely on the unique amino acid distribution associated with protein disorder \cite{uversky_what_2002,obradovic_exploiting_2005,dosztanyi_pairwise_2005}. Machine learning methods use either neural networks \cite{cheng_accurate_2005,jones_prediction_2003,linding_protein_2003} or support vector machines \cite{ward_prediction_2004,vullo_spritz:_2006} and are commonly based on sequence profiles, predicted secondary structure and more recently template structures \cite{mcguffin_intrinsic_2008}. More recently, meta servers combining several biophysical and machine learning methods have been published \cite{mizianty_improved_2010,xue_pondr-fit:_2010,schlessinger_improved_2009}. All these methods have shown promising results, possibly for two reasons: (i) as the amino acid sequence contains all the information to determine structure it is reasonable to assume that unstructured regions have specific amino acid propensities and (ii) disorder is important in many biological functions and therefore unstructured protein segments should be conserved by evolution. Knowing that disordered segments have a biased sequence, machine learning techniques should excel. In this paper we describe and benchmark CSpritz, an extension of our previous Spritz server \cite{vullo_spritz:_2006} based on three distinct modules for the prediction of intrinsically disordered regions in proteins. The performance of the method will be benchmarked on the latest available data for short and long disordered segments. A novel addition to the CSpritz server is information about homologous structures found from PSI-BLAST searches, secondary structure and linear motifs contributing to the functional annotation of disordered segments.

\section{Materials and methods}

CSpritz predicts intrinsic disorder from protein sequences through a combination of three machine learning systems, which will be described in the following sections. Most methods consider short and long disorder separately, as they have different characteristics. Short disorder can be derived from residues missing backbone atoms in X-ray crystallographic structures deposited in the Protein Data Bank (PDB) \cite{berman_worldwide_2007}. Long disorder is taken from the Disprot database \cite{sickmeier_disprot:_2007} because it is largely missing from the PDB. All data sets used throughout training are appropriately redundancy reduced using UniqueProt \cite{mika_uniqueprot:_2003} and in all cases contain only sequences available before May 2008 (i.e. the start of CASP8).

\subsection{Spritz}

The original Spritz \cite{vullo_spritz:_2006} is based on PSI-BLAST \cite{altschul_gapped_1997} multiple sequence profiles and predicted secondary structure. Support Vector Machines (SVMs) were used on a local sequence window to train two specialized binary classifiers, for long and short regions of disorder. A description of the data sets can be found in the previous publication \cite{vullo_spritz:_2006}. In addition to the original ab initio version of Spritz, a filter removing PDB structural homologues from predicted disorder is implemented. This works by performing a PSI-BLAST search against a redundancy reduced sequence database. The generated sequence profile is then used in a final PSI-BLAST round against a filtered PDB. Residues matching a structural template are assigned a Spritz score below the disorder threshold.

\subsection{Punch}

Punch is a SVM based predictor extending Spritz. Sequence and structural homologues are detected as in Spritz. In addition, Porter secondary structure \cite{pollastri_porter:_2005} and PaleAle relative solvent accessibility \cite{pollastri_accurate_2007} are also included. Unlike Spritz, information about structural templates is encoded and fed directly to the SVM together with the other inputs. The two data sets used for learning (see~\ref{sec:cspritzsupp}) are a large set of disordered X-ray chains derived from the PDB (December 2007) and a publicly available data set \cite{cheng_accurate_2005} based on disordered X-ray segments from the PDB (May 2004). The assignment of disorder is different in both data sets and does not necessarily intersect.

\subsection{ESpritz}

ESpritz is a fast predictor using bidirectional recursive neural networks (BRNNs) \cite{baldi_principled_2003}. BRNNs do not require contextual windows because they extract this information dynamically from the sequence. ESpritz consists of 20 inputs where each unit is allocated for one of the 20 amino acids. Although the method is very simple, the BRNN is useful for extracting relevant patterns required for disorder without the use of PSI-BLAST sequence alignments (results not shown). Like Spritz, two types of data based on long and short disorder types are designed (see~\ref{sec:cspritzsupp}). The short disorder set is built from X-ray PDB structures (May 2008). Long disorder segments are extracted from Disprot (version 3.7) with identical sequences removed.

\subsection{Linear motifs and secondary structure}

It can be useful to unify the following information for disordered segments: (i) amino acids involved; (ii) secondary structure; and (iii) important linear motifs. CSpritz offers this predicted information in various forms (see output section). Secondary structure propensities are predicted from Porter \cite{pollastri_porter:_2005}. Linear motifs (LMs) are selected from ELM \cite{gould_elm:_2010} as the ligand binding subset (names starting with LIG). ELM is a resource for predicting functional sites in eukaryotic proteins where functional sites are identified by patterns. These motifs are supposed to be representative of the more studied LM–protein binding examples. The selected LMs are returned when sub-sequences are matched by their regular expressions in ELM.

\section{Performance evaluation}
\subsection{Combination}

Experiments were carried out for the best procedure to combine Punch, Spritz and ESpritz. After trying majority voting, unanimous votes and combination with neural networks, the simplest method of averaging the probabilities produced by each system was found to be the best (data not shown). The optimal decision threshold was determined on data independent from the benchmarking set by maximizing the Sw measure \cite{noivirt-brik_assessment_2009}. CASP8 data \cite{noivirt-brik_assessment_2009} was used for short and Disprot (version 3.7) for long disorder. Regular expressions are incorporated to fill disordered regions separated by less than three residues. The Pearson correlation of the probabilities produced on CASP9 disorder targets was calculated to test how different the three predictors are. Table~\ref{tab:cspritz01} shows this correlation and proves that the three systems are indeed sufficiently different. This is important for combining the three systems since it is well known that ensembling predictions which are different or uncorrelated improve generalization performance considerably \cite{sollich_learning_1996}. In particular, combination is especially beneficial when the wrongly predicted residues for each predictor do not correlate (i.e. their probabilities do not correlate) \cite{albrecht_simple_2003,sirota_parameterization_2010}.

\begin{table}[ht]
    \centering
    \begin{tabular}{|l|l|l|l|}
        \hline
                & ESpritz & Spritz & Punch \\
        \hline
        ESpritz & 1.00    & 0.51   & 0.59  \\
        Spritz  &         & 1.00   & 0.42  \\
        Punch   &         &        & 1.00  \\
        \hline
        \end{tabular}
    \caption[Pearson correlation of the three systems on CASP9 targets]{Pearson correlation of the three systems on CASP9 targets. The probabilities are produced by each component on all residues for 117 CASP9 targets. Since the correlations are low, combining the three systems improves performance over the individual systems.}
    \label{tab:cspritz01}
\end{table}

\subsection{Benchmarking sets}

Validation of short disorder segments is performed on the 117 CASP9 targets\footnote{\url{http://www.predictioncenter.org/casp9/}}, comparing with other groups taking part in the disorder category experiment according to their official CASP results. In order to validate the long disorder segments we choose DisProt entries enriched with PDB annotation from the SL data set defined in \cite{sirota_parameterization_2010}. Unfortunately, selecting sequences with \textless40\% sequence identity to our training set leaves only 29 proteins. We also define a set of 569 X-ray sequences (Xray569) deposited in the PDB (resolution at most 2.5 Å and R-free \textless0.25) between May 2008 and September 2010 reduced by sequence identity using UniqueProt \cite{mika_uniqueprot:_2003} to an HSSP value of 0 to our training data and among each other. Supplementary Table~\ref{sec:cspritzsupp}.1 shows the size and composition of the validation data sets. Note that to ensure a fair comparison to other methods on our benchmarking sets, CSpritz was in all cases run with sequence and PDB databases frozen prior to May 2008.

\subsection{CASP short disorder}

To assess the performance of our server for the short disorder option, we rank all groups participating in the CASP9 experiment. Table~\ref{tab:cspritz02} shows the top 5 (out of 32) groups plus CSpritz and Spritz ranked by Sw, a commonly used measure at CASP. For Sw, as in the CASP8 assessment \cite{noivirt-brik_assessment_2009} the statistical significance of the evaluation scores was determined by bootstrapping: 80\% of the targets were randomly selected 1000 times, and the standard error of the scores was calculated (i.e. 1.96 * standard error gives 95\% confidence around mean for normal distributions). For a full list of rankings see the online methods page. Our results suggest a consistently good performance of our server, especially when taking into account that some of the top five are meta-servers and some are not publicly available.

\begin{table}[ht]
    \centering
    \begin{tabular}{|l|l|l|l|}
        \hline
        GroupID: Name         & Sw ($\pm$SE)      & ACC   & AUC   \\
        \hline
        291: PRDOS2           & 50.44 ($\pm$1.08) & 75.22 & 0.852 \\
        119: MULTICOM-REFINE  & 49.53 ($\pm$1.00) & 74.77 & 0.818 \\
        000: CSpritz          & 49.27 ($\pm$1.02) & 74.64 & 0.828 \\
        351: BIOMINE\_DR\_PDB & 48.21 ($\pm$1.25) & 74.11 & 0.818 \\
        374: GSMETADISORDERMD & 47.13 ($\pm$0.96) & 73.57 & 0.815 \\
        193: MASON            & 45.98 ($\pm$1.17) & 73.00 & 0.740 \\
        000: Spritz           & 24.91 ($\pm$1.18) & 62.46 & 0.716 \\
        \hline
    \end{tabular}
    \caption[CSpritz and Spritz compared to the top CASP9 results]{Results for the top five performing groups at the CASP9 experiment, CSpritz and the original Spritz. Disordered segments of less than three residues were removed (results unchanged if included, see Supplementary Table~\ref{sec:cspritzsupp}.3). The standard error (SE) for Sw is shown in brackets. ACC is the accuracy, i.e. (sensitivity + specificity)/2, and AUC the area under the receiver operator curve. A total of 32 groups participated in CASP9 disorder prediction category.}
    \label{tab:cspritz02}
\end{table}

\subsection{DisProt long disorder}

The long disorder type performance of CSpritz was benchmarked by comparing Sw, accuracy and AUC with the original Spritz and state-of-the-art predictors PONDR-FIT \cite{xue_pondr-fit:_2010}, Disopred \cite{ward_prediction_2004} and IUPred \cite{dosztanyi_pairwise_2005}. Table~\ref{tab:cspritz03} shows CSpritz performing significantly better than the other predictors for this type of disorder. In addition CSpritz improves over the long disorder predictions made by our previous server Spritz.

\begin{table}[ht]
    \centering
    \begin{tabular}{|l|l|l|l|}
        \hline
        Method          & Sw ($\pm$SE)      & ACC   & AUC   \\
        \hline
        CSpritz (short) & 54.64 ($\pm$3.58) & 77.32 & 0.837 \\
        CSpritz (long)  & 65.70 ($\pm$3.52) & 82.85 & 0.891 \\
        Spritz (short)  & 12.12 ($\pm$6.16) & 56.06 & 0.685 \\
        Spritz (long)   & 35.55 ($\pm$3.58) & 67.78 & 0.734 \\
        PONDR-FIT       & 51.53 ($\pm$4.34) & 75.77 & 0.817 \\
        Disopred2       & 46.20 ($\pm$4.00) & 73.10 & 0.806 \\
        IUPred (short)  & 37.65 ($\pm$4.77) & 68.83 & 0.814 \\
        IUPred (long)   & 42.57 ($\pm$4.75) & 71.29 & 0.818 \\
        \hline
    \end{tabular}
    \caption[Comparison for DisProt disordered regions]{Comparison for DisProt disordered regions. Spritz is compared with the original Spritz, PONDR-FIT, Disopred and IUPred. Where applicable both short and long options are reported. The standard error (SE) for Sw is shown in brackets. ACC is the accuracy, i.e. (sensitivity + specificity)/2, and AUC the area under the receiver operator curve. The decision threshold and best Sw was found to be 0.26 and 51.85 on the training set.}
    \label{tab:cspritz03}
\end{table}

\subsection{Large-scale performance}

To estimate the run time of CSpritz compared to others and validate the predictions on a larger set of PDB structures we use the Xray569 set. The results (Supplementary Table~\ref{sec:cspritzsupp}.2) are similar to the DisProt set and confirm the performance of CSpritz compared to the other methods. As can be expected, all methods are better at predicting disorder at the N- and C-termini than in the central part of the protein sequences. The execution time for CSpritz is largely determined by the PSI-BLAST search and comparable to the original Spritz and Disopred2, with ca. 15 min for an average protein. When executing multiple predictions, the CSpritz web server will run up to five proteins in parallel, reducing the overall time significantly.

\section{Server description}

The CSpritz input page is designed with simplicity in mind. A single or multiple sequences in FASTA format are the only input required and can be either pasted or uploaded as a file. Pasting is limited to 32000 characters but uploading has no restrictions. User email address and a query title are optional. Either short (default) or long disorder options can be selected, with the appropriate decision thresholds determined on data not involved in the benchmarking. To facilitate navigation, help and methods pages are available at the top of the interface.

The CSpritz output is presented in two main pages. The first page, displaying statistics, links to individual pages and a downloadable archive for all user supplied proteins, is present only if more than one sequence was submitted. A histogram of disordered segments and an archive for download containing all generated data are also available. Figure~\ref{fig:cspritz01} shows a sample global page for the 117 CASP9 targets.

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\textwidth]{cspritz_fig01}
    \caption[CSpritz output page for multiple sequences]{Global output page for multiple sequences. Summary statistics are displayed for some interesting values about the disorder segments of all query sequences. An archive is offered for download containing all disorder predictions, linear motifs and statistics for each protein the user supplied. The inset shows a graph displaying the length distribution of disorder segments among all proteins.}
    \label{fig:cspritz01}
\end{figure}

The second output displays predicted disorder and annotation for individual proteins. In addition to showing the sequence with predicted secondary structure and disorder, several statistics regarding the distribution of disorder are presented. An extensive description of the output is available as part of the online help page. Two graphs plot the probability of disorder and the number of available structural templates versus disordered regions in homologous PDB structures. The last part of the output concerns the presence of putative linear motifs and secondary structure propensity for disordered segments. This can be a useful source of functional annotation, as shown in Figure~\ref{fig:cspritz02} for Drosophila melanogaster Cryptochrome (dCRY). Following computational analysis, functional linear motifs were experimentally confirmed in the disordered C-terminus of dCRY \cite{hemsley_linear_2007}. CSpritz aims to speed up this type of analysis by providing additional clues. In dCRY the putative linear motifs (Figure~\ref{fig:cspritz02}) match the disordered residues having a favorable alpha helical propensity. It is known that many such interactions involve disorder to secondary structure transitions upon binding \cite{vanhee_protein-peptide_2009}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=1\textwidth]{cspritz_fig02}
    \caption[CSpritz output page for Drosophila Melanogaster Cryptochrome]{Individual output page for D. melanogaster Cryptochrome. The main figure shows the list of available files and actual disorder prediction. The latter is composed of the amino acid sequence, its predicted secondary structure and the CSpritz disorder classification, with disordered residues highlighted in red font. Disorder statistics about the protein is presented on the right. Two insets show the graphs for the disorder propensity plot (top right) and number of available structural coordinates versus disordered segments in homologous sequences. The inset on the bottom part shows the annotated disordered segment covering the C-terminus of Cryptochrome (residues 513–542). The propensities for secondary structure and location of putative functional motifs are shown. Links to the ELM description of the motif amino acids involved in the motif are supplied on the right. A graph and probabilities secondary structure propensity are also supplied.}
    \label{fig:cspritz02}
\end{figure}

\section{Conclusions}

We have described CSpritz, a novel web server for the prediction of intrinsically disordered protein segments from sequence. It allows the batch prediction of many sequences simultaneously, providing overview statistics. The single protein sequence is annotated with disorder and useful information regarding local secondary structure and possible interaction motifs, providing a first step towards the functional interpretation of disorder. Future work will concentrate on improving the functional description of disordered regions by including other types of related information such as repeats \cite{marsella_repetita:_2009} and aggregation \cite{trovato_pasta_2007}.
