# Computational Analysis and Annotation of Proteome Data: Sequence, Structure, Function and Interactions

With the advent of modern sequencing technologies, the amount of biological data
available has begun to challenge our ability to process it. The development of new
tools and methods has become essential for the production of results based on
such a vast amount of information. This thesis focuses on the development of
such computational tools and method for the study of protein data.

I first present the work done towards the understanding of intrinsic protein
disorder. Through the development of novel disorder predictors, we were able
to expand the available data sources to cover any protein of known sequence.
By storing these predicted annotations, together with data from other sources,
we created MobiDB, a resource that provides a comprehensive view of available
disorder annotations for a protein of interest, covering all sequences in the UniProt
database. Based on observations obtained from this resource, we proceeded to
create a data analysis workflow with the goal of furthering our understanding of
intrinsic protein disorder.

The second part focuses on tandem repeat proteins. The RAPHAEL method
was developed to assist in the identification of tandem repeat protein structures
from PDB files. Identified repeat structures were then manually classified into a
formal classification schema, and published as part of the RepeatsDB database.

Finally, I describe the development of network-based tools for the analysis of
protein data. RING allows the user to visualise and study the structure of a
protein as a network of nodes, linked by physico-chemical properties. The second
method, PANADA, enables the user to create protein similarity networks and to
assess the transferability of functional annotations between clusters of proteins.
