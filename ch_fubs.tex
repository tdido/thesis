\chapter{The many faces of intrinsic protein disorder}
\label{chap:fubs}

\section{Introduction}

\subsection{Structure and function of intrinsically disordered proteins}

As has already been discussed in previous chapters, intrinsic protein disorder has been a very active field of research over the last few decades. Experimental determination of the phenomenon, however, is rare, and it often involves conditions that are not natural to the protein. For example, large complexes may be broken down into parts to facilitate X-Ray diffraction experiments. These alterations can likely disrupt a protein's natural structure and/or function. It is possible that results influenced by such modifications have led us to the misinterpretation of many cases currently thought to be evidence of intrinsic protein disorder. In fact, it has recently been suggested that intrinsic protein disorder may be largely caused by artifacts of current methods for protein production~\cite{janin_protein_2013}. Instead of being intrinsically disordered and functional while in that state, the authors of the study suggest that, when in vivo, most disordered proteins are actually~\emph{proteins waiting for a partner} (PWPs). A PWP's disordered state is only transient and non-functional, and it will undergo folding upon binding its partner. Upon reaching its folded state, it will then be able to perform its function. The concept of folding upon binding is by no means novel, and it has been extensively documented~\cite{demarest_mutual_2002,keramisanou_disorder-order_2006,murzin_metamorphic_2008,babu_versatility_2012}. The ability of intrinsically disordered proteins to promiscuously bind many partners allows them to act as hub proteins, which are often involved in cellular regulatory and feedback mechanisms~\cite{babu_versatility_2012,munz_role_2012,ferreon_modulation_2013}. Independently of the chosen name, and of the possible existence false positives due to the previously mentioned artifacts, folding upon binding seems to play an important role in the function of proteins that are highly relevant from the biological point of view.

\subsection{The current situation of intrinsic disorder annotations}

Currently available intrinsic protein disorder annotations can be grouped into three categories: experimental, indirect, and predicted. As is usually the case, the higher the quality of the data, the harder it is to obtain. The quality/coverage tradeoff between the different categories is illustrated in Figure~\ref{fig:fubs01}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{fubs_fig01}
    \caption[The intrinsic protein disorder data pyramid]{The intrinsic protein disorder data pyramid. Available intrinsic protein disorder data sources present a serious tradeoff between quality and coverage. While, on one extreme, well documented annotations from literature are counted in the hundreds, automated predictors allow us to obtain annotations for any protein with a known sequence. For comparison purposes, the UniProt database features -at the time of writing- almost 50 million protein sequences.}
    \label{fig:fubs01}
\end{figure}

\subsubsection{Experimental annotations}

The de-facto standard for experimental annotations of disorder is the DisProt database~\cite{sickmeier_disprot:_2007}. DisProt is a highly valuable resource, containing curated annotations manually extracted from literature. A quick analysis of its contents, however, makes it evident that both the frequency of its updates and the number of new annotations on each release are seriously lagged when compared to the growth of protein data repositories such as the Protein Data Bank~\cite{berman_worldwide_2007} or the SwissProt database~\cite{consortium_ongoing_2011}.

\subsubsection{Indirect  annotations}

In order to make up for the limited coverage of experimentally obtained annotations, many research efforts turn to indirect methods for the generation of datasets. The most popular of these methods is X-Ray diffraction. When a protein structure is resolved by X-Ray diffraction, there are often regions where its backbone can not be accurately observed. One of the possible reasons for this is the fact that the region is disordered, and its position is therefore not easily determined by a method that has been devised for the observation of stable protein structures~\cite{brandt_seqatoms:_2008,janin_protein_2013}. An advantage of using this definition of disorder is that one can take advantage of the resources from the Protein Data Bank, and thus obtain a dataset of tens of thousands of proteins. A great disadvantage, however, is that missing regions in X-ray diffraction experiments are rather short, since by definition an X-Ray experiment strives to resolve as much structure as possible. The presence of very long, not observed regions, would be considered evidence of an unsuccessful experiment. Figure~\ref{fig:fubs06} provides an overview on the distribution of lengths that can be obtained by extracting the missing regions from PDB X-Ray experiments. It is clear that potentially interesting, longer disordered regions are obtained much less frequently with this approach. A second disadvantage is the fact that missing regions could be due to causes other than naturally occurring intrinsic disorder (for example low resolution of the X-Ray experiment, high B-factor, modifications of conditions (e.g. pH, temperature, or pressure) during the experiment)~\cite{janin_protein_2013}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{fubs_fig06}
    \caption[Length distribution of not observed regions in PDB structures]{Length distribution of not observed regions in PDB structures, clearly showing the over-representation of short segments.}
    \label{fig:fubs06}
\end{figure}

\subsubsection{Predicted annotations}

By using the previously described experimental and/or indirect annotations as training data, many automated predictors have been developed. Based mainly on mathematical/statistical models (e.g. machine learning methods), these tools present the immediate advantage of expanding the available dataset of annotations to all proteins of known sequence. As shown in Chapter~\ref{chap:cspritz} and Chapter~\ref{chap:espritz}, these predictors can very accurately detect disorder as defined by the available experimental and indirect annotations. It is worth mentioning, however, that the limitations of these annotations (scarcity of the experimental ones and fuzziness of the indirect ones) are inherently present in the predictors' output. For this reason, while they represent an extremely useful tool to guide research, predicted annotations should always be thought of as an approximation to disorder, and not as a precise determination of it.

\subsection{Dissecting available intrinsic protein disorder annotations}

While analysing some entries from the original release of the MobiDB database, which provides a quick overview of all available disorder annotations for any given protein (see Chapter~\ref{chap:mobidb}), we noticed several cases where a protein's disorder annotation from the DisProt database would be in conflict with a structure from the PDB. While DisProt would annotate a certain region as disordered, one could find a deposited structure on the PDB which covered the same region. Our hypothesis was that these conflicts could be caused by annotations of proteins that undergo folding upon binding: while DisProt would most likely be annotating the unfolded conformation, the PDB structure would have captured its bound and folded state. In order to test this hypothesis, we set out to analyse all cases where DisProt and the PDB exhibit such a conflict. By reviewing the literature associated with the proteins exhibiting this behaviour, we hoped to gain some understanding on the types of disorder that can be extracted from currently available disorder annotations, and to understand whether the observed conflicts could be an indication of folding upon binding.

\section{Materials and methods}

In order to obtain an overview of the relationship between DisProt and PDB annotations, we started by plotting the percentage of agreement between DisProt ``disorder'' annotations and PDB ``missing residue" annotations in the 287 cases where there was at least one residue in agreement. Results are shown in Figure~\ref{fig:fubs02}. Out of all the cases with agreement, in 42 of them the agreement between DisProt and PDB annotations reached 100\%. Upon reviewing the cited literature we found that, in the case of those 42 proteins, DisProt takes as its source of annotations publications that report missing residues from a crystallography experiment, thus effectively incorporating an annotation that is also part of the ``indirect'' dataset (itself, as mentioned before, made up of data obtained from PDB structures). In 50\% of these cases the DisProt entries feature a comment explaining that the annotations are ``based solely on missing electron density in the Protein Data Bank". In the other half of the cases no such clarification is made. Amongst these overlapping entries with no clarifying comment, some of them feature references to intrinsic protein disorder in the articles describing the X-Ray experiments (e.g. DP00235, DP00407). For the rest, no mention of intrinsic protein disorder is made (e.g. DP00248, DP00252, DP00422). Since by definition these perfectly agreeing DisProt entries are already present in the X-Ray dataset, we decided to exclude them entirely from all further analysis\footnote{Given potential differences in the numbering of residues between the PDB and DisProt, it is more than likely that entries that show an agreement slightly below 100\% will also be based solely on X-Ray experiments. For the sake of simplicity, however, we decided to exclude only those entries showing 100\% agreement.}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{fubs_fig02}
    \caption[Agreement between DisProt and the PDB]{Agreement between annotations in DisProt and the PDB. For those entries that feature both DisProt (disorder) and PDB (missing residues) annotations, the figure shows how well these two sources agree. An agreement of 100\% means that every single residue considered as disordered by DisProt, is missing from the X-Ray experiment.}
    \label{fig:fubs02}
\end{figure}

In addition to entries with perfect overlap, we additionally removed: entries where the sequence identity between the DisProt sequence and the PDB sequence was below 95\%; entries that have been replaced in UniProt and thus can not reliably link the DisProt and PDB annotations; and entries mapped to a UniProt chain and thus not independently annotated. Figure~\ref{fig:fubs07} provides a detailed view on the number of used and ignored entries. The final dataset, after filtering, contained 566 entries.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{fubs_fig07}
    \caption[Detail of ignored and used entries]{Detail of ignored and used entries. Redundant entries between datasets, or entries that can not reliably be mapped between them, were removed from the dataset.}
    \label{fig:fubs07}
\end{figure}

The next step was to perform a similar analysis for the case of conflicting annotations between DisProt and the PDB. The results are shown in Figure~\ref{fig:fubs03}. Over 300 entries feature at least some conflict between DisProt and PDB annotations. Under the assumption that interesting folding upon binding cases would occur at sequence regions with lengths that could accommodate functional domains, and in order to keep the number of entries manageable, we decided to include in the following steps only those entries with at least 30\% of their sequences in conflict. 

\section{Results and discussion}

After reviewing the literature referenced from each DisProt entry (as well as additional publications in case the cited source did not provide enough information), we were able to identify the intrinsic disorder related characteristics for each conflicting region. By grouping the entries into categories and subcategories, we were able to generate a simple classification tree, shown in Figure~\ref{fig:fubs04}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{fubs_fig03}
    \caption[Conflict between DisProt and the PDB]{Conflict between DisProt and the PDB. For those entries that feature both DisProt (disorder) and PDB (missing residues) annotations, the figure shows how much these sources are in conflict. A conflict of 100\% means that every single residue considered as disordered by DisProt, is shown as being part of a well-defined structure in at least one PDB structure. Entries whose conflict surpasses the horizontal line have more than 30\% of their sequence's residues in conflict.}
    \label{fig:fubs03}
\end{figure}

\begin{figure}[ht]
    \centering
        \Tree
        [.Disorder 
            [.Non-fis.conditions
                [.Exp.conditions ]
                [.Errors ]
            ]
            [.Complexes
                [.Ribosome ]
                [.Viral ]
                [.Other ]
            ]
            [.FuB  
                [.Metals  ]
                [.Lipids ]
                [.DNA ]
                [.PPI ]
            ]
        ]
    \caption[Classification of the 52 conflicting entries]{Classification of the 52 conflicting entries into a simple category/subcategory scheme, based on the type of disorder reported in literature.}
    \label{fig:fubs04}
\end{figure}

The simple classification obtained provides a starting point for the analysis of disorder in a more specific manner, steering away from the usual one-size-fits-all approach. The following sections describe the characteristics of the members assigned to each category.

\subsection{Non-physiological conditions}
This category includes those entries that we consider as non-relevant from the point of view of a biological analysis. It contains 8 entries.

\subsubsection{Experimental conditions}
Entries in this category have been subjected to experiments under conditions that differ from those normally encountered in a living organism. High temperature or pressure, and low pH are the most commonly found situations. As an example, DisProt entry DP00303 documents Myoglobin as being disordered based on an experiment performed at a pressure of 3000 bar. Acknowledging the accuracy of the annotation with regards to what has been reported in the citing article, we would argue that the inclusion of this annotation would work in detriment of any analysis done towards the understanding of intrinsic protein disorder in living organisms.

\subsubsection{Annotation errors}
These entries represent simple errors when transferring the reported annotations from the original publication. An example is DisProt entry DP00607, where Frataxin is reported as being disordered on the c-terminal half. The original report states that Frataxin is disordered in its n-terminus.

\subsection{Complexes}
The 15 entries under the ``complexes" category are documented to become disordered when they are studied in isolation from the complex to which they belong. Our subset mainly contained examples from the Ribosomal complex and viral complexes, with a few entries corresponding to other complexes. Even if this category would seem to have much in common with the ``protein-protein interaction" subcategory under ``folding upon binding", we have decided to assign it to its own category based on the fact that its members are part of well-known complexes with many components, are rarely thought of independently.

\subsection{Folding upon binding}
Entries in this section (29 in total) have been documented as existing in an intrinsically disordered state until binding a partner. These partners can range from simple metal ions to complex molecules, like other proteins or even nucleic acids. DisProt entry DP00028 provides an outstanding example of a protein-protein interaction induced folding. PDB structure 2jgb, shown in Figure~\ref{fig:fubs05}, features the otherwise intrinsically disordered 4EBP1 protein having acquired structure upon binding its partner.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{fubs_fig05}
    \caption[4EBP1 structured while bound to its partner]{4EBP1 structured while bound to its partner. Part A on top shows the PDB structure of 4EBP1 (structured regions in blue, disordered in red) having acquired structure when bound to its partner. Part B shows the corresponding annotations in the MobiDB database (\url{http://mobidb.bio.unipd.it/entries/Q13541}). Notice how all PDB experiments resolve the central $\alpha$-helix of 4EBP1, while the rest of the protein is either not observed (red pieces of sequence) or completely absent. DisProt annotates instead the unbound, fully disordered state. The combination of both sources generates a ``conflict" region, shown in green in the MobiDB consensus annotation.}
    \label{fig:fubs05}
\end{figure}

\section{Conclusions}

Intrinsic protein disorder annotations have been used extensively for different types of analyses over the course of the last few decades. These efforts, however, usually treat disorder as a single phenomenon. Starting from the hypothesis that intrinsic protein disorder actually encompasses a variety of related phenomena, we implemented a simple workflow for data analysis with the goal of finding subgroups corresponding to different disorder types. We demonstrated that as simple a feature as the conflict of two independent data sources can give out a signal indicating a feature as relevant as folding upon binding. This categorisation of intrinsic protein disorder should be easy to expand in order to cover a larger portion of the available annotations. More specific tools and methods, aware of the existence of the different categories, should in turn allow us to better understand the particulars of each subgroup, and therefore of intrinsic protein disorder.
