\chapter{Overview}
\label{chap:overview}

During the last two decades, with the advent of new sequencing technologies, the biological sciences have joined other fields in generating massive amounts of data at ever increasing rates~\cite{_data_2012,hess_microarrays:_2001,logares_environmental_2012}. While this \emph{data deluge} offers outstanding opportunities for novel research efforts and the advancement of the field, it also represents a great challenge in terms of our ability to store and analyse the generated data. If we add to the mix the tendency of data availability to decline as time progresses \cite{vines_availability_2013}, it is hard to ignore the fact that we are currently faced with a truly complex and multifaceted issue.

Bioinformatics is a field that combines elements from biology, computer science, engineering, mathematics, and statistics (among others), to tackle biological problems. It involves the development of tools and methods to aid on the storage, processing and analysis of biological data. Even though the origins of bioinformatics can be traced as far back as the 1970s~\cite{hesper_bioinformatica:_1970}, it is undeniable that the field has experienced an exponential growth that is coupled to the emergence of the aforementioned data deluge.

Owing its heterogeneous nature to its relationship to a field as varied as biology, the field of Bioinformatics deals with a wide range of topics. Three main groups of datasets have been proposed as being the main focus of the field: macromolecular structures, genome sequences, and results of functional genomics experiments~\cite{luscombe_what_2001}. This is of course a rather~\emph{coarse grained} definition, as the complexity of biological data defies the possibility of achieving a unique an precise categorisation. Figure~\ref{fig:overview01} offers a schematic view of the breadth and depth of bioinformatics, featuring examples of the types of data that can be of interest for bioinformatics analysis, and of the methods that can be applied to such data.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{overview_fig01}
    \caption[The Bioinformatics Spectrum]{The Bioinformatics Spectrum. A summarised, schematic view of the breadth and depth of bioinformatics~\cite{luscombe_what_2001,gerstein_what_1998}.}
    \label{fig:overview01}
\end{figure}

\section{Outline}

This thesis is divided into five parts, themselves subdivided into chapters. Part~\ref{part:intro} consists of an introduction and a general overview of the topics that are present in the different sections. Part~\ref{part:disorder} describes the work done on the analysis of intrinsic protein disorder. Part~\ref{part:repeats} presents results obtained from the study of tandem repeat proteins. Part~\ref{part:networks} describes the use of networks for the analysis of protein data. To conclude, Part~\ref{part:conclusions} presents conclusions and outlook of the obtained results.

It should be noted that most chapters included in parts~\ref{part:disorder},~\ref{part:repeats} and~\ref{part:networks} have been previously published in peer-reviewed journals, as specified at on their corresponding headings. It is also worth noting that I have used both ``I" and ``We" when describing the work done in different sections. This has the double objective of distinguishing the sections where work was done in collaboration with other researchers, and to differentiate my personal opinions from those of the group where the work was done.

\section{Computational methods and tools for the analysis and annotation of proteins}

The conversion of raw data into useful information, which will allow a researcher to arrive to valuable conclusions, is far from being a trivial task. Adding the fact that in the biological sciences this data has overwhelming volumes, it becomes clear how researchers face a formidable challenge. 

From a data-centric point of view, research can be though of as the execution of four main, intertwined steps: the generation of raw data, the conversion of raw data into processed data, the storage (and publication) of data, and the analysis of data. A research project may require anything from a single one to all of these steps, depending on previous availability, data complexity, research goals, etc. With the exception of the generation of raw data, all of the previously enumerated steps were covered during the development of this thesis.

The following overall description will provide some general insight on the topics of research that constitute the core of the research done throughout this thesis, and that will be described in detail in the upcoming chapters.

\subsection{Intrinsic protein disorder}

During the last few years, the traditional ``lock-and-key" paradigm of structural biology, where proteins interact with each other by the complementary nature of their well-defined structures, has been increasingly challenged. It is now generally accepted that there exists a group of proteins which can lack (in whole or in part) stable three-dimensional structure under certain conditions. These proteins are known as~\emph{intrinsically disordered proteins}\footnote{For consistency, I have chosen to use the term~\emph{intrinsically disordered protein} throughout this thesis. The variety of names under which the phenomenon can be found in literature evidences how even reaching an agreement on a naming convention is far from a simple task~\cite{orosz_proteins_2011}.}.

Chapter~\ref{chap:cspritz} and Chapter~\ref{chap:espritz} describe newly developed protein disorder predictors which, at the time of writing, constitute the state-of-the-art in the field. These predictors leverage machine learning methods to generate high-quality predictions for intrinsic protein disorder. Both predictors are based on bi-directional recursive neural networks (BRNNs). Unlike traditional neural networks, BRNNs do not learn the context of the sequence by applying a ``sliding window" (i.e. a fixed-length piece of sequence). Instead, they extract information implicitly through the recursive dynamics of the network.

Building on the results obtained from these predictors, Chapter~\ref{chap:mobidb} describes MobiDB, a database for intrinsic disorder annotation. Currently in its second major release, MobiDB uses state-of-the-art technology and a carefully developed modular design to provide a fast and comprehensive resource to the public. Its goal is to provide the best possible intrinsic protein disorder annotation for each of its entries, and it achieves this by aggregating information from different sources and by combining this information in a clever way. Covering the full set of proteins available in the UniProt knowledge base (roughly 50 million at the time of writing), it is the largest available repository of intrinsic protein disorder annotations.

Chapter~\ref{chap:disanalysis} provides a more detailed description of the annotations available in the original release of MobiDB. The analysis of agreement and disagreement between different sources motivated many of the improvements that were later introduced into the second major release of the database\footnote{Given that the second release of MobiDB was finished shortly before the time of writing, this thesis features a description and screen captures of the previous, original release. Chapter~\ref{chap:fubs} and the final conclusions presented in Chapter~\ref{chap:conclusions}, however, are based on the datasets that were eventually included in the new release}.

Based on the content of MobiDB, Chapter~\ref{chap:fubs} presents an attempt to critically analyse and expand our knowledge of intrinsic protein disorder. By leveraging the tools and methods described in previous chapters as well as external resources, we study the similarities and differences between different disorder annotation sources to study a particular type of disorder: folding upon binding. Proteins that undergo folding upon binding are known to be disordered until binding a partner. In addition to detecting such cases, we were able to develop an initial classification schema for subtypes of intrinsic disorder. This schema should prove useful for the further refinement of tools that deal with intrinsic protein disorder.

Post-translational modifications have often been associated with intrinsically disordered regions. Chapter~\ref{chap:rubi}, perhaps the least organic to this second part of the thesis, makes use of disorder annotations in an attempt to improve the accuracy when detecting ubiquitination sites.

\subsection{Tandem Repeat Proteins}

Like intrinsically disordered proteins, tandem repeat proteins fall into the category of ``non-globular proteins": proteins that instead of having a globular-shaped structure tend to be rather elongated. Unlike intrinsically disordered proteins, however, tandem repeat proteins have a well defined structure. Their elongation is due to their structure being formed by adjacent structural subunits highly similar to each other. In most cases of tandem repeats, these subunits depend on one another to fold.

Following a logic similar to that used for the study of intrinsic disorder, Chapter~\ref{chap:raphael} presents RAPHAEL, a tool for detecting tandem repeats from structure form the Protein Data Bank. Originally designed for the detection of solenoid repeats, RAPHAEL is actually capable of detecting a wide range of tandem repeat classes. By manually curating the set of candidates obtained from running RAPHAEL on the full Protein Data Bank, we generated a highly detailed dataset of repeat protein annotations.

The results of the previously described annotation effort were made available through RepeatsDB. Described on Chapter~\ref{chap:repeatsdb}, RepeatsDB presents a classification of repeat structures into an organised schema. Entries contained in RepeatsDB present different levels of annotations, ranging from being automatically detected as repeated, to highly detailed manual annotations that include start and end positions for each repeat unit.

\subsection{Network-based tools for the analysis of protein data}

More user-oriented than the previously described ones, this part presents tools aimed at the end user. Chapter~\ref{chap:ring} describes RING, a tool for the construction and analysis of residue interaction networks (RINs). RING allows the user to construct physico-chemically valid RINs interactively, and to analyse them using the Cytoscape software. PANADA, described in Chapter~\ref{chap:panada}, is a tool for the visualisation of protein similarity networks. The tool permits the user to visualise clusters of related proteins, and to assess the transferability of functional annotations.
